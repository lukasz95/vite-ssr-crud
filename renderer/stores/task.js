import { defineStore } from "pinia";

export const useTaskStore = defineStore('task', {
	state: () => {
		return { 
			taskList: [
				{
					id: 0,
					name: 'Task name 1',
					description: 'Task description 1'
				},
				{
					id: 1,
					name: 'Task name 2',
					description: 'Task description 2'
				},
				{
					id: 2,
					name: 'Task name 3',
					description: 'Task description 3'
				}
			],
			feedback: false
		}
	},
	getters: {
		getTaskList: (state) => state.taskList,
		getFeedback: (state) => state.feedback
	},
	actions: {
		addTask(task) {
			this.taskList = [
        {
          id: Math.floor(Math.random() * 100),
          name: task.taskName,
          description: task.taskDescription
        },
        ...this.taskList
      ];
			this.feedback = true
		},
		deleteTask(taskId) {
			this.taskList = this.taskList.filter(task => task.id !== taskId);
		},
		updateTask(task) {
			this.taskList = this.taskList.map(item => {
				if(item.id === task.taskId) {
					return {
						...item,
						name: task.taskName,
						description: task.taskDescription 
					}
				} else {
					return item;
				}
			})
		}
	}
}); 